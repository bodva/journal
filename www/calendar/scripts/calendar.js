$(document).ready(function(){
	$("#repeat-checkbox").click(function(e){
		$("#repeatcol").toggle();
		e.preventDefault;
	});
	$(".day").click(function(e){
		var number = $(this).parent().attr("number");
		var day = $(this).attr("day");
		var msg = "lesson "+number+" in "+day;
		$("#lesson-num").val(number);
		$("#day-name").val(day);
		$(".day").removeClass("selected");
		$(this).addClass('selected');
		// alert(msg);
	});

	$("#applychange").click(function(e){
		var number = $("#lesson-num").val();
		var day = $("#day-name").val();
		if ((day == "") || (number == "")) {
			alert("Будьласка оберіть клітинку");
			return false;
		} 
		var teacher = $("#select-teacher").val();
		var teacher2 = $("#select-teacher-2").val();
		var lectiontype = $("#select-lection-type").val();
		var room = $("#select-room").val();
		var groupid = $("#select-group").val();
		var weekid = $("#select-week").val();
		var subject = $("#select-subject").val();
		var repeatcol = $("#select-repeatcol").val();
		if (document.getElementById("repeat-checkbox").checked) {
			var repeat = "on";
		} else {
			var repeat = "off";
		}
		// alert(repeat);
		// alert(teacher);
		// alert(room);
		$.ajax({
            url: '/calendar/ajax/do.php',
            data: 'act=change&lesson='+number+'&day='+day+'&teacher='+teacher+'&teacher2='+teacher2+'&lectiontype='+lectiontype+'&room='+room+'&groupid='+groupid+'&weekid='+weekid+'&subject='+subject+'&repeat='+repeat+'&repeatcol='+repeatcol,
            dataType: "json",
            type: "GET",
            success: function (data, textStatus) {
            	var num = 1;
            	alert(data);
            	// $("#"+day+number).html(data.items[1].item.info.subject_name+"<br>"+data.room+"<br>"+data.lectiontype);
            	// $("#"+day+number).html(data.items[1].item.info.subject_name);
            	var html = "<b>"+data.items[1].item.info.subject_name+"</b> ";
            	html += "("+data.items[1].item.info.room_name+")<br>";
            	html += data.items[1].item.info.type_lesson_name+"<br/>";
            	html += data.items[1].item.info.teacher_name+"<br/>";
            	$(".selected").html(html);
	        }
	    });
		e.preventDefault;
		return false;
	});
	$("#earsebutton").click(function(e){
		var number = $("#lesson-num").val();
		var day = $("#day-name").val();
		if ((day == "") || (number == "")) {
			alert("Будьласка оберіть клітинку");
			return false;
		} 
		var groupid = $("#select-group").val();
		var weekid = $("#select-week").val();
		var repeatcol = $("#select-repeatcol").val();
		if (document.getElementById("repeat-checkbox").checked) {
			var repeat = "on";
		} else {
			var repeat = "off";
		}
		$.ajax({
            url: '/calendar/ajax/do.php',
            data: 'act=earse&lesson='+number+'&day='+day+'&groupid='+groupid+'&weekid='+weekid+'&repeat='+repeat+'&repeatcol='+repeatcol,
            dataType: "json",
            type: "GET",
            success: function (data, textStatus) {
            	// alert(data);
            	// $("#"+day+number).html(data.teacher+"<br>"+data.room+"<br>"+data.lectiontype);
            	$(".selected").html("&nbsp;");
	        }
	    });
		e.preventDefault;
		return false;
	})
});

function ChangeGroup () {
	var groupid = $("#select-group").val();
	var weekid = $("#select-week").val();
	$(".day").removeClass("selected");
	$(".week").hide();
	$(".group").hide();
	$(".week"+weekid+".group"+groupid).show();
	// alert(".week"+weekid+" .group"+groupid);
	// alert(weekid);
}

function ChangeWeek () {
	var groupid = $("#select-group").val();
	var weekid = $("#select-week").val();
	$(".day").removeClass("selected");
	$(".week").hide();
	$(".group").hide();
	$(".week"+weekid+".group"+groupid).show();
}