<script languge="javascript">

	// для случай цифровой оценки подбор правильного значения поля, по мере набора в нем значения уже реализован в самом объекте select, однако варианты поведения варьируются в зависимости от браузера
							//var objSel = document.forms[0].integer;

	var flagSelected=false;
	// перед началом задаем флаг выбора в значении false

	function SelectDefaultIntValue(){
		// если еще не было выбора в этом поле
		if (!flagSelected){
			document.forms[0].integer.selectedIndex = ((document.forms[0].integer.options.length-2) /4)*3;	
			flagSelected = true;
		}
		// по клику происходит выпадание списка, и выбранным устанавливается элемент находящийся на 3/4 высоты всего списка.
		// от общей длинны отнимается 2 для учета пустого элемента, и того, что нумерация начинается с нуля
		// после выбора устанавливаем флаг выбора в положение true
	}

	function SetIntValue(){
			//alert(document.forms[0].integer.selectedIndex);
			// document.forms[0].integer.options[document.forms[0].integer.selectedIndex].selected = true;

			// устанавливает флаг обозначающий что уже было выбрана какое либо значение
			flagSelected = true;
	}

	function SetFocusFractional(which){
		//alert(which);
		if ((which==190)||(which==188)||(which==110)){ // нажатие точки, запятой, и клавиши на циферной клавиатуре
			document.forms[0].fractional.focus(); // нужно указать соответствующую форму в документе, для первой, индекс 0
			// весь документ.элемент массива форм.имя объекта внутри
		}
		if (which==32) { // в случае нажатия пробела, перебрасываем на последние элемент
			document.forms[0].integer.selectedIndex = document.forms[0].integer.options.length-1;
		}

	}
	function SetFocusInteger(which){
		//alert(String.charCodeAt('.'));
		if ((which==190) ||(which==188)||(which==110)){
			document.forms[0].integer.focus();
		}
		if (which==32) { // в случае нажатия пробела, перебрасываем на последние элемент
			document.forms[0].fractional.selectedIndex = document.forms[0].integer.options.length-1;
		}
	}
</script>

<?php
	$type_id=6; // тут задать тип оценки. использованна база из самого журнала, где 6 оценки 1..100, а 1 это "Н" или "Н/ув"

	// конект к базе
 	$db_handle=mysql_connect('localhost','root','');
 	$db_found=mysql_select_db('journal',$db_handle);
	mysql_query("set names utf8;",$db_handle);
	$sql='SELECT value FROM mark_value WHERE mark_type_id='.$type_id;
	$result=mysql_query($sql,$db_handle);
	// print_r($result);
	// заполнение массива возможных вариантов оценки
	$marks=array();
 	while ($row=mysql_fetch_assoc($result)){
 			//print_r($row);
 			$marks[]=$row['value'];
 	}
?>


<form name="marks">
<select name="integer" onKeyUp="SetFocusFractional(event.which);" onClick="SelectDefaultIntValue();" onChange="SetIntValue();">
	<?php 
	//$max=100; 
		for ($i=1; $i<count($marks); $i++){
				echo '<option>'.$marks[$i].'</option>';
		}
		echo '<option selected>'.$marks[$i].'</option>';
		//echo '<option selected></option>';
	?>
</select>	
.
<select name="fractional" onkeyup="SetFocusInteger(event.which)" <?php if($type_id==1) echo 'style="display:none"';?>>
<!-- для оценок типа "Н" или "Н/ув" отключаем видимость второго поля -->
	<?php 
		for ($i=1; $i<count($marks); $i++){
			echo '<option>'.$marks[$i].'</option>';
		}
		echo '<option selected>'.$marks[$i].'</option>';
		//echo "<option selected></option>"; 
	?>
</select>
</form>