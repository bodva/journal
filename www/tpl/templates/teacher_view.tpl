{* Smarty *}

{*
<br><b>view_step</b>: {$view_step} <br />
*}

{if     $view_step=='show-input-form1'} {************************************************************}
               Для перегляду оцінок необхідно вибрати зі списку наступні параметри:
               <br/> <br/>
       <form method='POST' name='formView' action='index.php'>
               <b>Група | Вид занять | Дисципліна </b> <br /> 
               <font face='Courier New'>
               <select name="indices">
                  {html_options values=$indices output=$names}
               </select>
               </font>
        <input type="hidden" name="menuItem" value={$menuItem}>
        <input type='hidden' name='formTeacherView_1' value='1'>
        <br />     <br />
        <input type='submit' value='Відправити'>
        </form>
{elseif $view_step=='show-marks'} {************************************************************}


<body>

{literal}
  <style type="text/css">
   TD {
    empty-cells: show; /* Прячем пустые ячейки */
   }
  </style>
{/literal}

    <h2>
    {$year}/{$year+1}
    <br />
    П Е Р Е Г Л Я Д &nbsp;&nbsp; О Ц І Н О К
    </h2>

    <table border=0>
    <tr>
       <td align=right> <b>Група:</b>
       <td align=left> {$group_name}
    <tr>
       <td align=right> <b>Дисципліна:</b>
       <td align=left> {$subject_name}
    <tr>
       <td align=right> <b>Вид занять:</b>
       <td align=left> {$type_lesson_name}
    </table>
    <table width="*" border=1 cellspacing=1 cellpadding=1>
    {*******************************************
               T A B L E   H E A D E R
    ********************************************}
    <tr>
        <th> № <br /> з.п.
        <th> П.І.Б.
    {foreach from=$dates item=date}
        <th> {$date}
    {/foreach}
    {*******************************************
                 T A B L E   B O D Y
    ********************************************}
    {foreach name=eachrow from=$rows item=row}
    <tr>
        <td align=center>{$row.student_num+1}</td>
        <td>{$row.student_name}</td>
        {foreach from=$row.marks item=mark}
        <td align=center>
           {$mark}
        </td>
        {/foreach}
    </tr>
    {/foreach}
    </table>
</body>
</html>

{/if}