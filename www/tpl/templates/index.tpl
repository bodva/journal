{* Smarty *}

<html>
<TITLE> Онлайновый журнал успеваемости и посещаемости </TITLE>
<head>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
    <link  href="/calendar/styles/style.css" type="text/css" rel="Stylesheet" />
    <script src="/calendar/scripts/jquery/jquery.js" type="text/javascript"></script>
    <script src="/calendar/scripts/jquery/jquery-ui.js" type="text/javascript"></script>
    <script src="/calendar/scripts/calendar.js" type="text/javascript"></script>
</head>

<body>
    {include file='header.tpl'}
    <!--  ====================================================================================== -->
    <table width=100% height=80% border=1 CELLSPACING=0 FRAME=VOID >
       <!--  RULES=ROWS -->
    <tr>
       <td width=10% align=center valign=top>  <font size=2>
          {include file='menu.tpl'}
       <td align=center valign=top>
          {*
              Переменная menuItem устанавливается в menu.php зависимости от привилегий пользователя
          *}
          {if     $menuItem=='teacher_edit'}
               {include file='teacher_edit.tpl'}
          {elseif $menuItem=='teacher_view'}
               {include file='teacher_view.tpl'}
          {elseif $menuItem=='change_password'}
               {include file='change_password.tpl'}
          {elseif $menuItem=='schedule'}
               {include file='schedule.tpl'}
          {elseif $menuItem=='exit'}
               {include file='exit.tpl'}
          {elseif $menuItem=='login'}
               {include file='login.tpl'}
          {elseif $menuItem=='help'}
               {include file='help.tpl'}
          {else}
             {include file='hello.tpl'}
          {/if}
        </td>
    </table>
    {include file='footer.tpl'}
</body>
</html>
