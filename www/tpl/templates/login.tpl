{* Smarty *}

{literal}
<script type="text/javascript">
<!--
   function trim(stringToTrim) {
   	return stringToTrim.replace(/^\s+|\s+$/g,"");
   }
   function checkFields() {
        if(trim(document.formLogin.login.value)=="")
        {
               alert("Не вказаний логін");
               return false;
        }
        else if(trim(document.formLogin.paswd.value)=="")
        {
               alert("Не вказаний пароль");
               return false;
        }
        return true;
   }
// -->
</script>
{/literal}

<form method='post' name='formLogin' action='index.php' onsubmit='return checkFields();'>
<input type='hidden' name='formLogin_submitted' value='1'>
<input type='hidden' name='menuItem' value='login'>
<center>
{if $login_step=='wrong_login' || $login_step=='wrong_paswd'}
   <p class=errmsg> Невдалий вхід до системи (логін:'{$login}') через
   неправильно набраний <font color=red>
   {if $login_step=='wrong_login'}
                                       логін.
   {else}
                                       пароль.
   {/if}
                        </font>
   <br /> Повторіть, уважно набираючи логін і пароль з урахуванням мови і регістру символів.<br><br>
   Якщо Ви ще не зареєстровані, зверніться до адміністратора системи для реєстрації.
   <br><br>
   </p>
{/if}
{if $login_step=='login_OK'}
   {$login}, Ви успішно увійшли до Системи.
   <br /> Для подальшої роботи користуйтеся меню зліва.
{else}
<b>Вхід до системи</b><br>
<table >
  <tr>
    <td align='right'>   Логін:
    <td align='left'>    <input type='text' name ='login' size=32>
   <tr>               
    <td align='right'>   Пароль:
    <td align='left'>    <input type='password' name ='paswd' size=32>
   <tr>               
    <td align='right'>   <hr align='right' width=50%>Після заповнення полів<br> натисніть кнопку
    <td align='left' valign='top'>    <hr align='left' width=50%>
    <input type='submit' value='Відправити'>
</table>
{/if}
</form>
