{* Smarty *}
<table border="0" >
	<tr>
		<td valign="top">
			<div id="header" class="header">
				<div id="group-select" class="group-select">
					Група:
					<select id="select-group" onChange="ChangeGroup();">
						{foreach from=$groups item=group}
							<option value="{$group.id}">{$group.name}</option>
    					{/foreach}
					</select>
				</div>
				<div id="week-select" class="week-select">
					Тиждень:
					<select id="select-week" onChange="ChangeWeek();" style="width: 105px;">
						{foreach from=$typeWeeks item=typeWeek}
							<option value="{$typeWeek.id}">{$typeWeek.name}</option>
						{/foreach}
					</select>
				</div>
			</div>
		</td>
	</tr>
	<tr height="700px" valign="top">
		<td>
			<div id="cnt" class="cnt">
				<div id="calendar" class="calendar">
					<table border="2">
							<tr class="tbody" id="tbody">
								<td class="lesson-number">#</td>
								{foreach from=$weekDays item=day}
								<td>{$day.name}</td>
								{/foreach}
								
							</tr>
					</table>
					{foreach from=$groups item=group}
						{foreach from=$typeWeeks item=typeWeek}
							{if $typeWeek.id eq "1" && $group.id eq "1"}
							<table border="2" class="week week{$typeWeek.id} group group{$group.id}">
							{else}
							<table border="2" class="week week{$typeWeek.id} group group{$group.id}" style="display: none;">
							{/if}
								{foreach from=$lessonNums item=lessonNum}
									<tr class="number" number="{$lessonNum.id}" id="number{$lessonNum.id}">
										<td class="lesson-number">{$lessonNum.name}</td>
										{foreach from=$weekDays item=day}
											{if isset($schedule[$group.id][$typeWeek.id][$day.id][$lessonNum.id])}
												<td class="day" day="{$day.id}" id="">
													<b>{$schedule[$group.id][$typeWeek.id][$day.id][$lessonNum.id].subject_name}</b>
													({$schedule[$group.id][$typeWeek.id][$day.id][$lessonNum.id].room_name})<br>
													{$schedule[$group.id][$typeWeek.id][$day.id][$lessonNum.id].type_lesson_name}<br>
													{$schedule[$group.id][$typeWeek.id][$day.id][$lessonNum.id].teachers[0].teacher_name}<br>
													{$schedule[$group.id][$typeWeek.id][$day.id][$lessonNum.id].teachers[1].teacher_name}
												</td>
											{else}
												<td class="day" day="{$day.id}" id="">&nbsp;</td>
											{/if}
										{/foreach}										
									</tr>
								{/foreach}
							</table>
						{/foreach}
					{/foreach}
				</div>
				<div id="instruments" class="instruments">
					<input type="hidden" id="lesson-num" name="lesson-num" value="">
					<input type="hidden" id="day-name" name="day-name" value="">
					<table>
						<tr>
							<td>Викладач</td>
						</tr>
						<tr>
							<td>
								<select id="select-teacher" style="width: 220px;">
									{foreach from=$teachers item=teacher}
										<option value="{$teacher.id}">{$teacher.name}</option>
			    					{/foreach}
								</select>					
							</td>
						</tr>
						<tr>
							<td>
								<select id="select-teacher-2" style="width: 220px;">
									<option value="0">&nbsp;</option>
									{foreach from=$teachers item=teacher}
										<option value="{$teacher.id}">{$teacher.name}</option>
			    					{/foreach}
								</select>					
							</td>
						</tr>
						<tr>
							<td>Тип</td>
						</tr>
						<tr>
							<td>
								<select id="select-lection-type">
									{foreach from=$typeLessons item=type}
										<option value="{$type.id}">{$type.name}</option>
									{/foreach}
								</select>					
							</td>
						</tr>
						<tr>
							<td>Аудиторія</td>
						</tr>
						<tr>
							<td>
								<select id="select-room">
									{foreach from=$rooms item=room}
										<option value="{$room.id}">{$room.name}</option>
			    					{/foreach}
								</select>
							</td>
						</tr>
						<tr>
							<td>Предмет</td>
						</tr>
						<tr>
							<td>
								<select id="select-subject" style="width: 220px;">
									{foreach from=$subjects item=subject}
										<option value="{$subject.id}">{$subject.name}</option>
			    					{/foreach}
								</select>
							</td>
						</tr>
						<tr>
							<td>
								Повторювати <input id="repeat-checkbox" name="repeat-checkbox" type="checkbox"  checked="yes">
							</td>
						</tr>
						<tr id="repeatcol" class="repearcol" style="">
							<td>
								кожні
								<select id="select-repeatcol">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
								</select>
								тиждень
							</td>
						</tr>
						<tr>
							<td>
								<input type="submit" id="applychange" name="applycange"value="Додати"><br/>
								<input type="submit" id="earsebutton" name="earsebutton" value="Видалити">
							</td>
						</tr>
					</table>
				</div>
			</div>
		</td>
	</tr>
</table>