{* Smarty *}

{literal}
<script type="text/javascript">
<!--
   function trim(stringToTrim) {
   	return stringToTrim.replace(/^\s+|\s+$/g,"");
   }
   function checkFields() {
        if(trim(document.formChangePassword.oldlogin.value)=="")
        {
               alert("Не вказаний логін");
               return false;
        }
        else if(trim(document.formChangePassword.oldpswd.value)=="")
        {
               alert("Не вказаний пароль");
               return false;
        }
        else if(trim(document.formChangePassword.newlogin.value)=="" && trim(document.formChangePassword.newpswd1.value)=="")
        {
               alert("Не вказаний ні новий логін, ні новий пароль");
               return false;
        }
        else if(trim(document.formChangePassword.newpswd1.value)!="" && trim(document.formChangePassword.newpswd2.value)=="")
        {
               alert("Новий пароль мусить буди вказаний двічі");
               return false;
        }
        return true;
   }
// -->
</script>
{/literal}

<form method='post' name='formChangePassword' action='index.php' onsubmit='return checkFields();'>
<input type='hidden' name='formChangePassword_submitted' value='1'>
<input type='hidden' name='menuItem' value='change_password'>
<center>
{if     $ChangePassword_step=='wrong_oldlogin'}
   <p class=errmsg> Помилка: старий логін введено неправильно.<br>Зміни не внесено.<br> </p>
{elseif $ChangePassword_step=='wrong_oldpaswd'}
   <p class=errmsg> Помилка: старий пароль введено неправильно.<br>Зміни не внесено.<br> </p>
{elseif $ChangePassword_step=='wrong_newlogin_1'}
   <p class=errmsg> Помилка: новий логін мусить не співпадати зі старим.<br>Введіть ще раз.<br> </p>
{elseif $ChangePassword_step=='wrong_newlogin_2'}
   <p class=errmsg> Помилка: Ваш новий логін вже належить іншому користувачеві.<br>Введіть інший логін.<br> </p>
{elseif $ChangePassword_step=='wrong_newpaswd_1'}
   <p class=errmsg> Помилка: новий пароль мусить не  співпадати зі старим.<br>Введіть ще раз. </p>
{elseif $ChangePassword_step=='wrong_newpaswd_2'}
   <p class=errmsg> Помилка: повторне значення пароля введено неправильно.<br>Введіть ще раз.<br> </p>
{elseif $ChangePassword_step=='wrong_newpaswd_3'}
   <p class=errmsg> Помилка: і новий логін, і новий пароль порожні.<br>Зміни не внесено.<br> </p>
{/if}
{if $ChangePassword_step=='ChangePassword_OK'}
   Зміни успішно внесені<br>
{else}
<b>Зміна логіну та/або паролю</b><br><br>
<table width=100% >
  <tr>
    <td align='right'>   Старий логін:
    <td align='left'>    <input type='text' name ='oldlogin' size=32>
  <tr>
    <td align='right'>   Введіть новий логін:<br><font color='blue' size=2>(тільки у тому разі, якщо хочете<br>замінити старий логін)</font>
    <td align='left'>    <input type='text' name ='newlogin' size=32>
   <tr>               
    <td align='right'>   Старий пароль:
    <td align='left'>    <input type='password' name ='oldpswd' size=32>
   <tr>               
    <td align='right'>   Введіть новий пароль:<br><font color='blue' size=2>(тільки у тому разі, якщо хочете<br>замінити старий пароль)</font>
    <td align='left'>    <input type='password' name ='newpswd1' size=32>
   <tr>               
    <td align='right'>   Введіть ще раз новий пароль:
    <td align='left'>    <input type='password' name ='newpswd2' size=32>
   <tr>               
    <td align='right'>   <hr align='right' width=50%>Після заповнення полів<br> натисніть кнопку
    <td align='left' valign='top'>    <hr align='left' width=50%>
    <input type='submit' value='Відправити' onClick='return checkFields()'>
</table>
{/if}
</form>
