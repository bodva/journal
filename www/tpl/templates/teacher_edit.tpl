{* Smarty *}

{*
<br><b>edit_step</b>: {$edit_step} <br />
*}

{if     $edit_step=='show-input-form1'} {************************************************************}
               Для редагування оцінок необхідно уточнити наступні параметри:
               <br/> <br/>
       <form method='POST' name='formEdit' action='index.php' onsubmit='return confirmDbUpdate()'>
               <b>Тип оцінки</b>   <br />
               <select name="mark_type_id">
                  {html_options values=$mark_type_id output=$mark_type_name}
               </select>
               <br /> <br />
               
               <b>Дата | Пара | Група | Дисципліна </b>  <br />
               <font face='Courier New'>
               <select name="schedule_id">
                  {html_options values=$schedule_id output=$date_name}
               </select>
               </font>
    <input type="hidden" name="menuItem" value={$menuItem}>
    <input type='hidden' name='formTeacherEdit_1' value='1'>
    <br /> <br />
    <input type='submit' value='Відправити'>
        </form>
{elseif $edit_step=='show-input-form2'} {************************************************************}

{report recordset=$edit_table record=rec}
{report_header}
{*
   <html>
   <TITLE> Онлайновый журнал успеваемости и посещаемости </TITLE>
   <head>
      <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1251">
   </head>
*}
{literal}
<script type="text/javascript">
<!--
  function confirmDbUpdate() {
     if(confirm("Ви упевнені, що хочете зберігти зміни до бази даних?"))
        return true;
     else
        return false;
  }
// -->
</script>
{/literal}

<body>

    <h2>
    {$year}/{$year+1}
    <br />
    В И С Т А В Л Е Н Н Я &nbsp;&nbsp; О Ц І Н О К
    </h2>
    <table border=0>
    <tr>
       <td align=right> <b>Викладач:</b>
       <td align=left> {$teacher_name}
    <tr>
       <td align=right> <b>Вид оцінки:</b>
       <td align=left> {$mark_type_name}
    <tr>
       <td align=right> <b>Дата, пара:</b>
       <td align=left> {$date_name}
    </table>
    <form method='POST' name='formEdit' action='index.php' onsubmit='return confirmDbUpdate()'>
    <table width="400" border=1 cellspacing=1 cellpadding=1>
    <tr>
        <th> № <br /> з.п.
        <th> П.І.Б.
        <th> Оцінка
{/report_header}
{report_detail}
    <tr>
        <td align=center>{$rec.student_num+1}</td>
        <td>{$rec.student_name}</td>
            {if $rec.mark_type_is_blocked}
        <td align=center bgcolor=yellow  title='заблоковано іншою оцінкою'>
               <label>{$rec.mark_value}</label>
            {else}
        <td align=center>
               <select name="mark_{$rec.student_num}">
                  {html_options values=$mark_values output=$mark_values selected=$rec.mark_value}
               </select>
            {/if}
        </td>
    </tr>
{/report_detail}
{report_footer}
    </table>
    <input type="hidden" name="menuItem" value={$menuItem}>
    <input type='hidden' name='formTeacherEdit_submitted' value='1'>
    <input type='submit' value='Зберігти'>
    </form>
{/report_footer}
</body>
</html>
{/report}

{elseif $edit_step=='save-to-database'} {************************************************************}
  Інформацію успішно збережено в базі даних.

{/if}