<?php
class Edit
{
 private
      $smarty, $idlink, $functions;
 public 
   function __construct(&$smarty_object, &$idlink)
   {
      require_once('functions.php');
      $this->functions = new Functions();
      $this->smarty = $smarty_object;
      $this->idlink = $idlink;
   }
   function execute()
   {
      //global $smarty;
      //session_start();
//      echo __FILE__.' line# '.__LINE__.' function '.__FUNCTION__."<br />";        // debug 2011-09-17 17:03
      if(isset($_POST['formTeacherEdit_submitted']))
         $edit_step = 'save-to-database' ;
      elseif(isset($_POST['formTeacherEdit_1']))
         $edit_step = 'show-input-form2' ;
      else
         $edit_step = 'show-input-form1' ;
      //$edit_step = isset($_POST['formTeacherEdit_submitted']) ? 'save-to-database' : 'show-input-form' ;
      //unset($_POST['form_submitted']);
      //$edit_step = 'show-input-form';
      switch ($edit_step){
         case 'show-input-form1':
                  $this->print_form1();
                  break;
         case 'show-input-form2':
                  $this->print_form2();
                  break;
         case 'save-to-database':
                  $this->saveToDatabase();
                  break;
      }
      $this->smarty->assign('edit_step',$edit_step);
   }
 private
   function print_form1()
   {
//      echo __FILE__.' line# '.__LINE__.' function '.__FUNCTION__."<br />";        // debug 2011-09-17 17:03

      // Тип оценки
      $sql = "SELECT id,name FROM mark_type ORDER BY name" ;
      $r = mysql_query($sql, $this->idlink);
      for($a=array(), $b=array(); $row=mysql_fetch_array($r); $a[]=$row['id'], $b[]=$row['name']);
      $this->smarty->assign('mark_type_id', $a);
      $this->smarty->assign('mark_type_name', $b);

      // Дата, пара
      $user_rec = $_SESSION['user_rec'];
      $user_id = $user_rec['id'];
      $teacher_rec = $this->functions->get_rec('teacher', "user_id=$user_id");
      $teacher_id = $teacher_rec['id'];
      //var_dump(__FILE__.' line '.__LINE__, $user_rec);
      //var_dump(__FILE__.' line '.__LINE__, $teacher_rec);
      $sql = "SELECT
                     schedule.id AS schedule_id,
                     type_week.name AS type_week_name,
                     week_day.name AS week_day_name,
                     lesson_num.name AS lesson_num_name,
                     journal.group.name AS group_name,
                     subject.name AS subject_name,
                     week_day.id AS week_day_id,
                     room.name AS room_name,
                     type_lesson.name AS type_lesson_name
              FROM schedule, type_week, week_day, lesson_num, subject, journal.group, room, type_lesson
              WHERE 
                     schedule.teacher_id='$teacher_id'
                     AND  schedule.type_week_id = type_week.id
                     AND  schedule.week_day_id = week_day.id
                     AND  schedule.lesson_num_id = lesson_num.id
                     AND  schedule.subject_id = subject.id
                     AND  schedule.group_id = journal.group.id
                     AND  schedule.room_id = room.id
                     AND  schedule.type_lesson_id = type_lesson.id
              ORDER BY type_week.name, week_day.id, lesson_num.name;" ;
      $r = mysql_query($sql, $this->idlink);
      $this->functions->my_die($r, 'Invalid query');

      for($a=array(), $b=array(); $row=mysql_fetch_array($r);)
      {
         $d = $this->functions->getScheduleDate($row['type_week_name'],$row['week_day_id']);
         $sd = date('Y-m-d', $d);
         $sd2 = $sd.$row['lesson_num_name'].$row['group_name'];
         $a[$sd2]=$row['schedule_id'];
         $b[$sd2]=$row['type_week_name'].
                ' | '.$sd.
                ' | '.$row['week_day_name'].
                ' | '.$row['lesson_num_name'].
                ' | '.$row['room_name'].
                ' | '.$row['group_name'].
                ' | '.substr($row['type_lesson_name'], 0, 3).
                ' | '.$row['subject_name'];
      };
      ksort($a);   ksort($b);      // упорядочиваем по дате и номеру пары
      $this->smarty->assign('schedule_id', $a);
      $this->smarty->assign('date_name', $b);
      $_SESSION['schedule_id'] = $a;    // на следующей форме понадобится
      $_SESSION['date_name']   = $b;    // на следующей форме понадобится
      //var_dump(__FILE__.' line '.__LINE__, $a);
      
   }
   function print_form2()
   {
//      echo __FILE__.' line# '.__LINE__.' function '.__FUNCTION__."<br />";        // debug 2011-09-17 17:03
      $schedule_id  = $_POST['schedule_id'];
      $mark_type_id = $_POST['mark_type_id']; 
      $mark_type_id_attendance  = $this->functions->get_rec_field('mark_type', 'is_attendance=1', 'id');
      //echo "schedule_id=$schedule_id<br>";
      //echo "mark_type_id=$mark_type_id<br>";
      //echo "mark_type_id_attendance=$mark_type_id_attendance<br>";
      //$schedule_id  = 1;   // debug 2011-08-21 07:39
      //$mark_type_id = 2;   // debug 2011-08-24 19:36 тип: КнР
      //$mark_type_id_attendance   = 7;   // debug 2011-08-25 10:40 тип: Посещаемость
      //
      //  Найти нужную ячейку расписания
      //
      $sql = "SELECT * FROM schedule WHERE id=$schedule_id;";
      $r = mysql_query($sql,$this->idlink);
      //for($data=array(); $row=mysql_fetch_array($r); $data[]=$row);
      //print_r($data);
      $row=mysql_fetch_array($r);
      if(!$row)
         throw new Exception('запись с id='.$schedule_id.' не найдена в таблице schedule');
      $group_id = $row['group_id'];
      //
      // Найти ФИО преподавателя и тип оценки
      //
      $teacher_id = $row['teacher_id'];
      $teacher_name  = $this->functions->get_rec_field('teacher', "id=$teacher_id", 'name');
      $mark_type_name  = $this->functions->get_rec_field('mark_type', "id=$mark_type_id", 'name');
      //
      // Сформировать запрос к БД для редактирования оценок указанной группы (ячейки расписания)
      //
      //
      //      Сначала получаем весь список студентов этой группы...
      //
      $sql = "SELECT student.id AS student_id,student.name FROM student 
              WHERE student.group_id=$group_id
              ORDER BY student.name;";
      $r = mysql_query($sql,$this->idlink);
      for($stud_list=array(); $row=mysql_fetch_array($r); $stud_list[]=$row);
      //var_dump('Список студентов ($stud_list):',$stud_list); // 2011-08-27 20:09 debug
      //
      //      ... затем получаем список оценок ДАННОГО ТИПА ($mark_type_id) для тех студентов, у которых уже были оценки за эту ячейку расписания
      //
      $sql = "SELECT student.id AS student_id,
                     mark.id AS mark_id, mark.mark_type_id AS mark_type_id, mark.mark_value_id,
                     mark_value.value AS mark_value
              FROM student, mark, mark_value
              WHERE mark.schedule_id=$schedule_id AND student.id=mark.student_id
                    AND (mark.mark_type_id=$mark_type_id OR mark.mark_type_id=$mark_type_id_attendance)
                    AND mark_value.id=mark.mark_value_id
              ORDER BY student.name;";
      $r = mysql_query($sql,$this->idlink);
      for($mark_list=array(); $row=mysql_fetch_array($r); $mark_list[$row['student_id']]=$row);
      //var_dump('Список оценок ($mark_list):', $mark_list);          // 2011-08-27 20:09 debug
      //
      //      ... затем получаем КОЛИЧЕСТВО оценок ПРОТИВОПОЛОЖНОГО ТИПА (противоположности - это ПОСЕЩАЕМОСТЬ и НЕ ПОСЕЩАЕМОСТЬ)
      //
      $condition_mark_type_id_contrary = ($mark_type_id==$mark_type_id_attendance)?
                                          "mark.mark_type_id<>$mark_type_id_attendance" :
                                          "mark.mark_type_id=$mark_type_id_attendance" ;
      $sql = "SELECT student.id AS student_id, count(student.id) AS count
              FROM student, mark
              WHERE mark.schedule_id=$schedule_id AND student.id=mark.student_id
                    AND $condition_mark_type_id_contrary
              GROUP BY student.id, mark.schedule_id
              ORDER BY student.name;";
      $r = mysql_query($sql,$this->idlink);
      $this->functions->my_die($r, 'Invalid query');

      for($mark_count_list=array(); $row=mysql_fetch_array($r); $mark_count_list[$row['student_id']]=$row['count']);
      //echo $sql."<br />";
      // var_dump('Список количеств "противоположных" оценок  ($mark_count_list):', $mark_count_list);   // 2011-08-27 20:09 debug
      //
      //      ... затем получаем список всех возможных оценок ДАННОГО ТИПА
      //
      $sql = "SELECT mark_value.id,mark_value.value  FROM mark_value
              WHERE mark_value.mark_type_id=$mark_type_id
              ORDER BY convert(mark_value.value,UNSIGNED) DESC;";
      $r = mysql_query($sql,$this->idlink);
      for($mark_values=array(); $row=mysql_fetch_array($r); $mark_values[$row['value']]=$row['id']);
      //var_dump('Всевозможные значения оценок ($mark_values):', $mark_values);   // 2011-08-27 20:09 debug
      //
      //      ... и сливаем результаты запроса в один массив
      //
      $edit_table = array();
      for($isize=count($stud_list), $i=0;   $i<$isize;   $i++)
      {
         $edit_table[$i]['student_num']  = "".$i;                  // номер студента в списке (временный номер, используется для индексирования в массивах сохранённых ID)
         $edit_table[$i]['student_name'] = $stud_list[$i]['name']; // ФИО студента
         $edit_table[$i]['mark_value']   = '';                     // значение оценки (если ='', то оценки нет (пока))
         $edit_table[$i]['mark_type_is_blocked'] = FALSE;          // тип оценки (это надо, чтобы избежать выставления оценки отсутствовавшему студенту)
         $student_id = $stud_list[$i]['student_id'];
         if(isset($mark_list[$student_id]))          // у данного студента уже была выставлена оценка ДАННОГО типа или отмечена посещаемость
         {
            $edit_table[$i]['mark_value'] = $mark_list[$student_id]['mark_value']; 
         }
         if(isset($mark_count_list[$student_id]))    // у данного студента была уже выставлена оценка ПРОТИВОПОЛОЖНОГО типа
         {
            $edit_table[$i]['mark_type_is_blocked'] = TRUE;
         }
      }
      //var_dump('edit_table:',$edit_table);   // 2011-08-27 20:09 debug
      //
      // Результат запроса передаём генератору отчётов (smarty)
      //
      global $config;
      $this->smarty->assign('year',$config->year);
      $this->smarty->assign('edit_table',$edit_table);
      $this->smarty->assign('mark_values',array_keys($mark_values));
      $this->smarty->assign('teacher_name',$teacher_name);
      $this->smarty->assign('mark_type_name',$mark_type_name);
      $a = $_SESSION['schedule_id'];
      //var_dump($a);
      $b = $_SESSION['date_name'];
      //var_dump($b);
      $k = @array_search($schedule_id, $a);
      $date_name = @$b[$k];
      $this->smarty->assign('date_name',$date_name);

      //
      // ID студентов и оценок сохраняем в качестве переменных сессии
      //
      $students_and_marks = array();
      for($isize=count($stud_list),$i=0;   $i<$isize;   $i++)
      {
         $student_id = $stud_list[$i]['student_id'];
         $students_and_marks[$i]['student_id'] = $student_id;
         $students_and_marks[$i]['mark_id']    = '';
         $students_and_marks[$i]['mark_value'] = '';
         if(isset($mark_list[$student_id]))
         {
            $students_and_marks[$i]['mark_id'] = $mark_list[$student_id]['mark_id'];
            $students_and_marks[$i]['mark_value'] = $mark_list[$student_id]['mark_value'];
         }
      }
      $_SESSION['students_and_marks'] = $students_and_marks;
      $_SESSION['mark_type_id'] = $mark_type_id;
      $_SESSION['schedule_id']  = $schedule_id;
      $_SESSION['mark_values']  = $mark_values;
   }
   function saveToDatabase()
   {
//      echo __FILE__.' line# '.__LINE__.' function '.__FUNCTION__."<br />";        // debug 2011-09-17 17:03
      // echo "saveToDatabase<br />";   // 2011-08-27 20:30 debug
      $students_and_marks = $_SESSION['students_and_marks'];
      $mark_type_id       = $_SESSION['mark_type_id'];
      $schedule_id        = $_SESSION['schedule_id'];
      $mark_values        = $_SESSION['mark_values'];
      //var_dump('students_and_marks', $students_and_marks);   // 2011-08-27 20:09 debug
      //var_dump('mark_values', $mark_values);   // 2011-08-27 20:09 debug

      //
      // Получаем список оценок от пользователя
      //
      foreach($students_and_marks as $stud_no => $one_student)      // Для каждого SELECT'а на HTML-форме
      {
         if(isset($_POST['mark_'.$stud_no]))
         {
            $new_mark = trim($_POST['mark_'.$stud_no]);
            $old_mark = trim($one_student['mark_value']);
            //echo $stud_no.')'. $new_mark.'<br />';   // 2011-08-27 20:30 debug
            if($old_mark!=$new_mark)                                // Оценка изменилась?
            {
               //echo "Оценка изменилась: было '$old_mark', стало '$new_mark' <br />";    // 2011-08-27 20:30 debug
               $mark_id = $one_student['mark_id'];
               $student_id= $one_student['student_id'];
               //
               // Оценка изменилась - изменяем БД
               //
               if($new_mark=='')                                    // Если пользователь хочет удалить оценку
               {                                                    // То удаляем её из БД
                  //echo "Удаляем оценку $old_mark<br />";   // 2011-08-27 20:30 debug
                  $sql = "DELETE FROM mark WHERE mark.id=$mark_id;";
                  if(!($r = mysql_query($sql)))
                     throw new Exception("не получилось удалить запись (id=$mark_id) из таблицы mark (".mysql_error().')');
               }
               else
               {                                                    // Если хочет поставить, то выстявляем...
                  $new_mark_value_id = $mark_values[$new_mark];
                  if($old_mark=='')                                      // Если оценки раньше не было
                  {                                                      // То добавляем её в БД
                     //echo "Добавляем оценку '$new_mark'<br />";   // 2011-08-27 20:30 debug
                     $sql = "INSERT INTO mark (	schedule_id,  student_id,  mark_value_id,      mark_type_id)
                                       VALUES ( $schedule_id, $student_id, $new_mark_value_id, $mark_type_id);";
                     if(!($r = mysql_query($sql)))
                        throw new Exception("не получилось вставить запись в таблицу mark (".mysql_error().')');
                  }
                  else
                  {                                                      // Если была, то изменяем её в БД
                     //echo "Заменяем оценку '$old_mark' на '$new_mark'<br />";   // 2011-08-27 20:30 debug
                     $sql = "UPDATE mark SET mark.mark_value_id=$new_mark_value_id WHERE mark.id=$mark_id;";
                     // echo $sql.'<br />';   // 2011-08-27 20:30 debug
                     if(!($r = mysql_query($sql)))
                        throw new Exception("не получилось изменить запись (id=$mark_id) из таблицы mark (".mysql_error().')');
                  }
               }
            }
         }
      }
   }
}
?>
