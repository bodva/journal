<?php 
class schedule {
private
	$smarty, $idlink, $functions;
public 
	function __construct(&$smarty_object, &$idlink)
		{
		require_once('functions.php');
		$this->functions = new Functions();
		$this->smarty = $smarty_object;
    	$this->idlink = $idlink;
   }

   function execute(){
		$this->showSchedule();
   }

   function showSchedule(){
    $groups = $this->getGroups();
    // print_r($groups[0]['schedule'][1]);
   	$this->smarty->assign("groups", $groups);
   	$this->smarty->assign("weekDays", $this->getWeekDay());
   	$this->smarty->assign("teachers", $this->getTeachers());
   	$this->smarty->assign("rooms", $this->getRooms());
   	$this->smarty->assign("typeLessons", $this->getTypeLessons());
   	$this->smarty->assign("subjects", $this->getSubjects());
    $this->smarty->assign("typeWeeks", $this->getTypeWeeks());
   	$this->smarty->assign("lessonNums", $this->getLessonsNums());
   	$this->smarty->assign("schedule", $this->getSchedule());
   }

  function getGroups(){
      $sql = "SELECT `id`,`name` FROM `group`" ;
      $r = mysql_query($sql, $this->idlink);
      $items = array();
      while ($row=mysql_fetch_array($r)) {
        $item = array();
        $item['id'] = $row['id'];
        $item['name'] = $row['name'];
        // $item['schedule'] = $this->getSchedule($item['id']);
        $items[] = $item;
      }
      return $items;
  }
	function getLessonsNums(){
   		$sql = "SELECT `id`,`name` FROM `lesson_num`" ;
    	$r = mysql_query($sql, $this->idlink);
    	$items = array();
    	while ($row=mysql_fetch_array($r)) {
    		$item = array();
    		$item['id'] = $row['id'];
    		$item['name'] = $row['name'];
    		// $item['schedule'] = $this->getSchedule($item['id']);
    		$items[] = $item;
    	}
    	return $items;
	}

  function parseSchedule($items){
    $schedule = array('');
    // group id
    // week num
    // day
    // lesson
    $i = 0;
    foreach ($items as $item) {
      $i++;
      $teachers = array();
      $teacher = array();
      $teacher['teacher_id'] = $item['teacher_id'];
      $teacher['teacher_name'] = $item['teacher_name'];
      $teachers[] = $teacher;        
      if (!empty($schedule[$item['group_id']][$item['type_week_id']][$item['week_day_id']][$item['lesson_num_id']])){
        $teacher['teacher_id'] = $schedule[$item['group_id']][$item['type_week_id']][$item['week_day_id']][$item['lesson_num_id']]['teachers'][0]['teacher_id'];
        $teacher['teacher_name'] = $schedule[$item['group_id']][$item['type_week_id']][$item['week_day_id']][$item['lesson_num_id']]['teachers'][0]['teacher_name'];
        $teachers[] = $teacher;
      } else {
        $schedule[$item['group_id']][$item['type_week_id']][$item['week_day_id']][$item['lesson_num_id']] = $item ;
      }
      $schedule[$item['group_id']][$item['type_week_id']][$item['week_day_id']][$item['lesson_num_id']]['teachers'] = $teachers;
      unset($schedule[$item['group_id']][$item['type_week_id']][$item['week_day_id']][$item['lesson_num_id']]['teacher_id']);
      unset($schedule[$item['group_id']][$item['type_week_id']][$item['week_day_id']][$item['lesson_num_id']]['teacher_name']);
    }
    // print $i;
    print_r("<pre>"); 
    // print_r($schedule); 
    // print_r(count($schedule,true));
    print_r("</pre>"); 
    return $schedule;
  }
  
  function getSchedule() {
  // function getSchedule($group_id) {
      $sql = "SELECT 
         n0.id, 
         n0.type_week_id, 
         n1.name as type_week_name, 
         n0.week_day_id, 
         n2.name as week_day_name, 
         n0.subject_id, 
         n3.name as subject_name, 
         n0.teacher_id, 
         n4.name as teacher_name, 
         n0.group_id, 
         n5.name as group_name, 
         n0.room_id, 
         n6.name as room_name, 
         n0.type_lesson_id, 
         n7.name as type_lesson_name, 
         n0.lesson_num_id, 
         n8.name as lesson_num_name 
         FROM `schedule` as n0 
         LEFT JOIN `type_week` as n1 ON n1.id = n0.type_week_id 
         LEFT JOIN `week_day` as n2 ON n2.id = n0.week_day_id 
         LEFT JOIN `subject` as n3 ON n3.id = n0.subject_id 
         LEFT JOIN `teacher` as n4 ON n4.id = n0.teacher_id 
         LEFT JOIN `group` as n5 ON n5.id = n0.group_id 
         LEFT JOIN `room` as n6 ON n6.id = n0.room_id 
         LEFT JOIN `type_lesson` as n7 ON n7.id = n0.type_lesson_id 
         LEFT JOIN `lesson_num` as n8 ON n8.id = n0.lesson_num_id 
         ORDER BY n1.id, n2.id, n0.group_id" ;
      // print_r($sql);
      $r = mysql_query($sql, $this->idlink);
      $items = array();
      while ($row=mysql_fetch_assoc($r)) {
        $items[] =  $row;
      }
      print_r("<pre>");
      // print_r($items);
      print_r("</pre>");
      // print_r("<br/>");
      $items = $this->parseSchedule($items);
      return $items; 
  }


	function getSubjects(){
    	$sql = "SELECT id,name FROM `subject` ORDER BY `id`" ;
    	$r = mysql_query($sql, $this->idlink);
    	$items = array();
    	while ($row=mysql_fetch_array($r)) {
    		$item = array();
    		$item['id'] = $row['id'];
    		$item['name'] = $row['name'];
    		$items[] = $item;
    	}
    	return $items;
	}


	function getWeekDay(){
   		$sql = "SELECT id,name FROM `week_day` ORDER BY `id`" ;
    	$r = mysql_query($sql, $this->idlink);
    	$items = array();
    	while ($row=mysql_fetch_array($r)) {
    		$item = array();
    		$item['id'] = $row['id'];
    		$item['name'] = $row['name'];
    		$items[] = $item;
    	}
    	return $items;
	}

	function getTypeWeeks(){
   		$sql = "SELECT id,name FROM `type_week` ORDER BY `id`" ;
    	$r = mysql_query($sql, $this->idlink);
    	$items = array();
    	while ($row=mysql_fetch_array($r)) {
    		$item = array();
    		$item['id'] = $row['id'];
    		$item['name'] = $row['name'];
    		$items[] = $item;
    	}
    	return $items;
	}

	function getTypeLessons(){
   		$sql = "SELECT id,name FROM `type_lesson` ORDER BY `id`" ;
    	$r = mysql_query($sql, $this->idlink);
    	$items = array();
    	while ($row=mysql_fetch_array($r)) {
    		$item = array();
    		$item['id'] = $row['id'];
    		$item['name'] = $row['name'];
    		$items[] = $item;
    	}
    	return $items;
	}

	function getTeachers(){
   		$sql = "SELECT id,name FROM `teacher` ORDER BY `id`" ;
    	$r = mysql_query($sql, $this->idlink);
    	$items = array();
    	while ($row=mysql_fetch_array($r)) {
    		$item = array();
    		$item['id'] = $row['id'];
    		$item['name'] = $row['name'];
    		$items[] = $item;
    	}
    	return $items;
	}

	function getRooms(){
   		$sql = "SELECT id,name FROM `room` ORDER BY `id`" ;
    	$r = mysql_query($sql, $this->idlink);
    	$items = array();
    	while ($row=mysql_fetch_array($r)) {
    		$item = array();
    		$item['id'] = $row['id'];
    		$item['name'] = $row['name'];
    		$items[] = $item;
    	}
    	return $items;
	}


}
 ?>