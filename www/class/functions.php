<?php
Class Functions
{
 public
   function my_die($idlink, $msg)
   {
      if(!$idlink)
          die("$msg:<br />". mysql_error());
   }

   function get_rec($tbl_name, $where_expression)
   {
      if (!empty($where_expression)){
         $sql = "SELECT * FROM `$tbl_name` WHERE $where_expression" ;
      } else {
         $sql = "SELECT * FROM `$tbl_name`";
      }
      $r = mysql_query($sql);
      if(!$r)
         echo "\"$sql\" ".mysql_error()."<br />";
      return mysql_fetch_assoc($r);
   }

   function get_rec_field($tbl_name, $where_expression, $fld_name)
   {
      $r = $this->get_rec($tbl_name, $where_expression);
      return $r[$fld_name];
   }

   function getScheduleDate($nomer_nedeli, $nomer_den_nedeli)
   {
      //
      // Функция возвращает календарную дату,
      // сответствующую заданной неделе и дню недели.
      // Дата начала семестра извлекается из глобальной конфигурации.
      //
      // $nomer_nedeli     - это номер учебной недели (внимание!: границы недели не совпадают с границами календарной недели)
      // $nomer_den_nedeli - это номер дня недели (внимание!: это календарный номер дня недели, 1-понедельник, 2-вторник, ...)
      //
      global $config;
      //require_once('config.php');   // для автономной отладки
      //$config = new Config;

      if($config->semestr1len>=$nomer_nedeli)             // это 1-й семестр?
         $semestrStart = $config->semestr1start;
      else                                                // тогда это 2-й семестр
         $semestrStart = $config->semestr2start;

      $d = @strtotime($semestrStart);
      $dw_start = 0+@date('w', $d); if($dw_start==0) $dw_start = 7;   // день недели, когда начался семестр (1-понедельник, ...)

      $nomer_den_nedeli = $nomer_den_nedeli-$dw_start+1; // преобразуем номер дня недели в номер дня учебной недели;
      if($nomer_den_nedeli<=0) // например: 1-й день 1-й учебной недели в 2011/2012 уч.г. - это четверг (4-й день календарной недели)
         $nomer_den_nedeli = $nomer_den_nedeli+7;
      $offset_days = ($nomer_nedeli-1)*7 + $nomer_den_nedeli - 1;

      //@date_add($d, date_interval_create_from_date_string(''.$offset_days.' days'));
      $d = $d + 86400*$offset_days;  // 86400=60*60*24 - длина 1 дня в секундах (в соответствии с форматом timestamp)
      return $d;

   }
}

// Test
/*
$functions = new Functions;
$d = $functions->getScheduleDate(1, 4);    // в 2011 г. - это 1 сентября, четверг на первой неделе (учебной)
echo @date('Y-m-d', $d)."\n";
$d = $functions->getScheduleDate(1, 5);    // в 2011 г. - это 2 сентября, пятница на первой неделе (учебной)
echo @date('Y-m-d', $d)."\n";
$d = $functions->getScheduleDate(2, 1);    // в 2011 г. - это 12 сентября, понедельник на второй неделе (учебной)
echo @date('Y-m-d', $d)."\n";
*/
?>
