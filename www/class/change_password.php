<?php
class ChangePassword
{
 private
      $smarty, $idlink, $functions;
 public 
   function __construct(&$smarty_object, &$idlink)
   {
      require_once('class/functions.php');
      $this->functions = new Functions;
      $this->smarty = $smarty_object;
      $this->idlink = $idlink;
   }
   function execute()
   {
      //echo __FUNCTION__.' line '.__LINE__.'<br />';        // debug1
      if(!isset($_POST['formChangePassword_submitted']))            // пользователь ещё не меняет пароль, только увидел форму
      {
         // в этом случае - только Smarty
      }
      else                                                 // попытка изменить пароль
      {
         $user_rec = $_SESSION['user_rec'];
         $user_id = $user_rec['id'];

         $oldlogin  =$_POST['oldlogin'];
         $newlogin  =$_POST['newlogin'];
         $oldpswd  =$_POST['oldpswd'];
         $newpswd1 =$_POST['newpswd1'];
         $newpswd2 =$_POST['newpswd2'];
         $user = $this->functions->get_rec('users', "id='$user_id'");
         //debug(__FUNCTION__.$oldlogin.' '.$newlogin.' '.$oldpswd.' '.$newpswd1.' '.$newpswd2.' '.$a['login'].' '.$a['password']);

         if($user['login']!=$oldlogin)                                     // неправильный логин
         {
            $this->smarty->assign('ChangePassword_step', 'wrong_oldlogin');
            return;
         }
         if(trim($user['password'])!==md5($oldpswd))
         {
            $this->smarty->assign('ChangePassword_step', 'wrong_oldpaswd');
            return;
         }
         if($newlogin && $oldlogin==$newlogin)
         {
            $this->smarty->assign('ChangePassword_step', 'wrong_newlogin_1');
            return;
         }
         if($this->functions->get_rec('users', "login='$newlogin'"))
         {
            $this->smarty->assign('ChangePassword_step', 'wrong_newlogin_2');
            return;
         }
         if($newpswd1 && $oldpswd==$newpswd2)
         {
            $this->smarty->assign('ChangePassword_step', 'wrong_newpaswd_1');
            return;
         }
         if($newpswd1 && $newpswd1!=$newpswd2)
         {
            $this->smarty->assign('ChangePassword_step', 'wrong_newpaswd_2');
            return;
         }
         if(trim($newlogin)==="" && trim($newpswd1)==="")
         {
            $this->smarty->assign('ChangePassword_step', 'wrong_newpaswd_3');
            return;
         }
         $sql = "UPDATE users SET";
         if($newlogin)
            $sql = $sql." login='".$newlogin."'";
         if($newpswd1)
         {
            if($newlogin)
               $sql = $sql.","; 
            $sql = $sql." password='".md5($newpswd1)."'";
         }
         $sql = $sql." WHERE id=".$user_id.";";
         $r = mysql_query($sql,$this->idlink);
         if($r)
            $this->smarty->assign('ChangePassword_step', 'ChangePassword_OK');
         else
            $this->functions->my_die($r, 'Зміни не внесені');

         $_SESSION['user_rec'] = $user;
      }
      //var_dump(__FILE__ ,$_SESSION['user_rec']);
   }
}

?>
