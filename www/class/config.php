<?php

class Config
{
 public
    //
    // В случае необходимости изменять здесь
    //
    $year          = '2011',        // уч. год. обозначается тем, календарным годом, в котором начинается учебный год
                                    // (например: 2011 для 2011/2012 уч.г.)                                          
                                    // значение '' означает, что система будет брать текущий учебный год             
    $semestr1start = '2000-09-01',  // здесь год значения не имеет (может быть любым, он берётся из переменной $year)
    $semestr1len   = 18,            // это максимально возможное количество недель в ПЕРВОМ семестре (используется для того, чтобы отличить расписание 1-го семестра от расписания 2-го семестра)
    $semestr2start = '2000-02-01',
    $semestr2len   = 18;
    // -------------------- ниже не изменять! 

    function __construct()
    {
       if($this->year==='')
       {
          $this->year = idate('Y');
          if(idate('m')<9)
             $this->year--;
       }
       $this->semestr1start = ''.($this->year+0).'-'.substr($this->semestr1start,5);
       $this->semestr2start = ''.($this->year+1).'-'.substr($this->semestr2start,5);
    }
}

/*
// Test
$config = new Config();
echo "{$config->year}\n{$config->semestr1start}\n{$config->semestr2start}\n";
*/
?>