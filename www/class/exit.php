<?php
   // Очистить данные сессии для текущего сценария
   $_SESSION = array();
   // Удалить cookie, соответствующую SID
   unset($_COOKIE[session_name()]);
   // Уничтожить хранилище сессии
   session_destroy();
?>
