<?php
class Menu
{
 private
      $smarty, $idlink;
 public 
   function __construct(&$smarty_object, $idlink)
   {
      $this->smarty = $smarty_object;
      $this->idlink = $idlink;
   }
   function execute($menuItem)
   {
      $user_rec = isset($_SESSION['user_rec']) ? $_SESSION['user_rec'] : '';  // если пользователь уже залогинился
      //var_dump(__FILE__.' line '.__LINE__,$_SESSION['user_rec']);

      //
      // Выполняется старое меню ...
      //
      if($user_rec)                              // Пользователь залогинился?
      {
         if(!$user_rec['is_teacher'])
         {
                                                // Як мінімум, студент
            switch($menuItem){
            case 'student_view':
                     require_once 'class/student_view.php';
                     $view = new studentView($this->smarty,$this->idlink);
                     $view->execute();
                     break;
            case 'change_password':
                     require_once 'class/change_password.php';
                     $a = new ChangePassword($this->smarty,$this->idlink);
                     $a->execute();
                     break;
            case 'exit':
                     require_once 'class/exit.php';
                     break;
            default:
                     $menuItem = '';   // если злоумышленник хакнул menuItem, мы всё равно не позволим ему
                                       // даже высветить (при момощи Smarty) соответствующую форму на экране (не то что выполнить)
            }
         }
         else                          // это преподаватель
         {
            switch($menuItem){
            case 'teacher_edit':
                     require_once 'class/teacher_edit.php';
                     $edit = new Edit($this->smarty,$this->idlink);
                     $edit->execute();
                     break;
            case 'teacher_view':
                     require_once 'class/teacher_view.php';
                     $edit = new View($this->smarty,$this->idlink);
                     $edit->execute();
                     break;
            case 'change_password':
                     require_once 'class/change_password.php';
                     $a = new ChangePassword($this->smarty,$this->idlink);
                     $a->execute();
                     break;
            case 'schedule':
                     require_once 'class/schedule.php';
                     $a = new schedule($this->smarty,$this->idlink);
                     $a->execute();
                     break;
            case 'exit':
                     require_once 'class/exit.php';
                     break;
            default:
                     $menuItem = '';   // если злоумышленник хакнул menuItem, мы всё равно не позволим ему
                                       // даже высветить (при момощи Smarty) соответствующую форму на экране (не то что выполнить)
            }
         }
      }
      else                                       // это гость
      {
         switch($menuItem){
         case 'login':
                  require_once 'class/login.php';
                  $login = new Login($this->smarty,$this->idlink);
                  $login->execute();
                  break;
         case 'help':
                  break;
         default:
                  $menuItem = '';   // если злоумышленник хакнул menuItem, мы всё равно не позволим ему
                                    // даже высветить (при момощи Smarty) соответствующую форму на экране (не то что выполнить)
         }
      }
      //
      // ... а отображается новое меню (возможно изменённое в результате выполнения пунктов предыдущего меню)
      //
      $menu_list = array();

      $user_rec = isset($_SESSION['user_rec']) ? $_SESSION['user_rec'] : '';  // если пользователь уже залогинился
      //var_dump(__FILE__.' line '.__LINE__,$_SESSION['user_rec']);     // debug
      if($user_rec)                              // Пользователь залогинился?
      {
         if(!$user_rec['is_teacher'])
         {
            $menu_list[] = array('action'=>'\'student_view\'', 'name'=>'Переглянути');
            $menu_list[] = array('action'=>'\'change_password\'', 'name'=>'Змінити пароль');
         }
         else                          // это преподаватель
         {
            $menu_list[] = array('action'=>'\'teacher_edit\'', 'name'=>'Редагувати');
            $menu_list[] = array('action'=>'\'teacher_view\'', 'name'=>'Переглянути');
            $menu_list[] = array('action'=>'\'change_password\'', 'name'=>'Змінити пароль');
            $menu_list[] = array('action'=>'\'schedule\'', 'name'=>'Розклад');
         }
         $menu_list[] = array('action'=>'\'exit\'', 'name'=>'Вийти');
      }
      else                                       // это гость
      {
         $menu_list[] = array('action'=>'\'login\'', 'name'=>'Увійти');
      }
      $menu_list[] = array('action'=>'\'help\'', 'name'=>'Довідка');

      $this->smarty->assign('menu_list',$menu_list);
   }
}

?>
