<?php
class View
{
 private
      $smarty, $idlink, $functions;
 public 
   function __construct(&$smarty_object, &$idlink)
   {
      require_once('functions.php');
      $this->functions = new Functions();
      $this->smarty = $smarty_object;
      $this->idlink = $idlink;
   }
   function execute()
   {
      //global $smarty;
      //session_start();
//      echo __FILE__.' line# '.__LINE__.' function '.__FUNCTION__."<br />";        // debug 2011-09-17 17:03
      if(isset($_POST['formTeacherView_1']))
         $view_step = 'show-marks' ;
      else
         $view_step = 'show-input-form1' ;
      switch ($view_step){
         case 'show-input-form1':
                  $this->print_form1();
                  break;
         case 'show-marks':
                  $this->print_marks();
                  break;
      }
      $this->smarty->assign('view_step',$view_step);
   }
 private
   function print_form1()
   {
//      echo __FILE__.' line# '.__LINE__.' function '.__FUNCTION__."<br />";        // debug 2011-09-17 17:03

      // Група, вид занять, дисципліна
      $user_rec = $_SESSION['user_rec'];
      $user_id = $user_rec['id'];
      $teacher_rec = $this->functions->get_rec('teacher', "user_id=$user_id");
      $teacher_id = $teacher_rec['id'];
      //var_dump(__FILE__.' line '.__LINE__, $user_rec);
      //var_dump(__FILE__.' line '.__LINE__, $teacher_rec);
      $sql = "SELECT DISTINCT
                     journal.group.id AS group_id,
                     type_lesson.id AS type_lesson_id,
                     subject.id AS subject_id,
                     journal.group.name AS group_name,
                     type_lesson.name AS type_lesson_name,
                     subject.name AS subject_name
              FROM schedule, journal.group, subject, type_lesson
              WHERE 
                         schedule.teacher_id='$teacher_id'
                     AND schedule.group_id=journal.group.id
                     AND schedule.type_lesson_id=type_lesson.id
                     AND schedule.subject_id=subject.id
              ORDER BY journal.group.name, subject.name, type_lesson.name;" ;
      $r = mysql_query($sql, $this->idlink);
      $this->functions->my_die($r, 'Invalid query');

      for($i=0, $a=array(), $b=array(); $row=mysql_fetch_array($r); $i++)
      {
         $ind[$i] = $i;
         $a[$i]  = array('group_id'=>$row['group_id'], 'type_lesson_id'=>$row['type_lesson_id'], 'subject_id'=>$row['subject_id'],
                         'group_name'=>$row['group_name'], 'type_lesson_name'=>$row['type_lesson_name'], 'subject_name'=>$row['subject_name']
                        );
         $b[$i]  =    $row['group_name'].
                ' | '.substr($row['type_lesson_name'], 0, 3).
                ' | '.$row['subject_name'];
      };
      $this->smarty->assign('indices', $ind);
      $this->smarty->assign('names', $b);
      $_SESSION['ids_and_names']     = $a;    // на следующей форме понадобится
      //var_dump(__FILE__.' line '.__LINE__, $a);
      
   }
   function print_marks()
   {
//      echo __FILE__.' line# '.__LINE__.' function '.__FUNCTION__."<br />";        // debug 2011-09-17 17:03
      $index  = $_POST['indices'];
      $a = $_SESSION['ids_and_names'];
            $group_id = $a[$index]['group_id'];
      $type_lesson_id = $a[$index]['type_lesson_id'];
          $subject_id = $a[$index]['subject_id'];
            $group_name = $a[$index]['group_name'];
      $type_lesson_name = $a[$index]['type_lesson_name'];
          $subject_name = $a[$index]['subject_name'];
      //var_dump('array a:', $a[$index]);
      //
      //  Найти всех студентов данной группы
      //
      $sql = "SELECT
                   student.id AS student_id,
                   student.name AS student_name
              FROM
                   student
              WHERE
                   student.group_id=$group_id
              ;";
      $r = mysql_query($sql,$this->idlink);
      $this->functions->my_die($r, __FILE__.' line# '.__LINE__.' function '.__FUNCTION__.'/'.mysql_error());
      for($t=array(), $i=0;  $row=mysql_fetch_array($r); $i++)
      {
         $t[$row['student_id']]['student_name']=$row['student_name'];
         $t[$row['student_id']]['student_num']=$i;
      }
      //var_dump('dump 1',$t);
      //
      //  Найти все даты занятий
      //
      $sql = "SELECT
                   type_week.name AS type_week_name,
                   week_day.id AS week_day_id
              FROM
                   schedule, type_week, week_day
              WHERE
                       schedule.group_id=$group_id
                   AND schedule.subject_id=$subject_id
                   AND schedule.type_lesson_id=$type_lesson_id
                   AND schedule.type_week_id=type_week.id
                   AND schedule.week_day_id=week_day.id
              ;";
      $r = mysql_query($sql,$this->idlink);
      $this->functions->my_die($r, __FILE__.' line# '.__LINE__.' function '.__FUNCTION__.'/'.mysql_error());
      for($dates=array() ;  $row=mysql_fetch_array($r); )
      {
         $d = $this->functions->getScheduleDate($row['type_week_name'],$row['week_day_id']);
         $dates[$d] = '';
      }
      // каждому студенту выставить для начала пустые оценки (но по каждой дате, то есть на каждом занятии)
      /*
      foreach($t as &$row)
      {
         $row['marks'] = $dates;
      }
      */                                  // к сожалению, работает с ошибкой не смотря на "&"
      foreach($t as $key=>$row)
      {
         $t[$key]['marks'] = $dates;
      }
      //var_dump('dump 2',$t);
      //
      //  Найти все оценки данной группы по данному виду занятий по данному предмету
      //
      $sql = "SELECT
                   student.id AS student_id,
                   student.name AS student_name,
                   mark_value.value AS mark_value,
                   mark_type.name AS mark_type_name,
                   type_week.name AS type_week_name,
                   week_day.id AS week_day_id,
                   mark_type.is_attendance AS is_attendance
              FROM
                   mark, schedule, student, mark_value, mark_type, type_week, week_day
              WHERE
                       mark.schedule_id=schedule.id
                   AND schedule.group_id=$group_id
                   AND schedule.subject_id=$subject_id
                   AND schedule.type_lesson_id=$type_lesson_id
                   AND mark.student_id=student.id
                   AND schedule.type_week_id = type_week.id
                   AND schedule.week_day_id = week_day.id
                   AND mark.mark_value_id = mark_value.id
                   AND mark_value.mark_type_id = mark_type.id
              ;";
      $r = mysql_query($sql,$this->idlink);
      if(!$r)
         $this->functions->my_die($r, __FILE__.' line# '.__LINE__.' function '.__FUNCTION__.'/'.mysql_error());
      //for($data=array(); $row=mysql_fetch_array($r); $data[]=$row);
      //print_r($data);
      for( ; $row=mysql_fetch_array($r); )
      {
         //var_dump('dump цикл for',$row);
         $d = $this->functions->getScheduleDate($row['type_week_name'],$row['week_day_id']);
         $t[$row['student_id']]['marks'][$d] .= $row['mark_value'];
         if(!$row['is_attendance'])
            $t[$row['student_id']]['marks'][$d] .= mb_strtolower(substr($row['mark_type_name'],0,1));
      }
      //var_dump('dump 3',$t);
      //
      // Расставлям даты в заголовке таблицы (массив $dates)
      //
      foreach($dates as $k=>&$v)
      {
         $v = date('d', $k)."<br />".date('m', $k);
      }
      
      //
      // Результат запроса передаём генератору отчётов (smarty)
      //
      global $config;
      $this->smarty->assign('year',$config->year);
      $this->smarty->assign('group_name',$group_name);
      $this->smarty->assign('subject_name',$subject_name);
      $this->smarty->assign('type_lesson_name',$type_lesson_name);

      $this->smarty->assign('dates',$dates);
      $this->smarty->assign('rows',$t);
   }
}
?>
